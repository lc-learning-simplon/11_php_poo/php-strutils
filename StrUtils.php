<?php

class StrUtils {

    private $str;

    public function __construct($n) {
        $this->str = $n;
    }

    public function getString() {
        echo $this->str;
    }

    public function bold() {
        $this->str = "<strong>" . $this->str . "</strong>";
    }

    public function italic() {
        $this->str = "<i>" . $this->str . "</i>";
    }

    public function underline() {
        $this->str = "<u>" . $this->str . "</u>";
    }

    public function capitalize() {
        $this->str = strtoupper($this->str);
    }

    public function uglify() {
        $this->bold();
        $this->italic();
        $this->underline();
    }
}


?>